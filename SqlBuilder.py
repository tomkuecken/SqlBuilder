import pandas
import csv
import re
import os
import xlrd

from enum import Enum


class SqlBuilder(object):
    @staticmethod
    def create_table_from_header(
            table_name,
            database_name,
            header,
            schema_name='',
    ):

        if not table_name.strip():
            return

        sql = ['\nUSE {0};\nGO\n'.format(database_name)]

        insert_name = '{0}.{1}.{2}'.format(database_name, schema_name, table_name)

        sql.append(str('IF OBJECT_ID(\'{0}\') IS NOT NULL\n'
                       '\tDROP TABLE {0};\nGO\n').format(insert_name))

        # Create TableNameID as primary key
        id_name = '{0}ID'.format(table_name)
        # Make sure primary key name is unique
        while id_name in header:
            import uuid
            id_name = '{0}_{1}'.format(id_name, uuid.uuid1().clock_seq_loq)

        sql.append('CREATE TABLE {0} (\n'
                   '\t[{1}] INT PRIMARY KEY IDENTITY(1, 1)\n'.format(insert_name, id_name))

        for col in header:
            sql.append('\t,[{0}] {1}\n'.format(col, 'NVARCHAR(MAX)'))
        sql.append(')\n')

        sql.append('\nGO\n')

        return ''.join(sql)

    @staticmethod
    def __get_insert_value(val: str, is_quoted: bool, is_null_to_empty_string=False):
        """
        Description:
            Pass in a single value (one column for one row)
            Any chars needing to be escaped are escaped
            Tests the value for encoding errors with StringIO
        :param val: str
            Value you're passing in
        :param is_quoted:
            Whether the value needs to be surrounded by single quotes
            Example:
                INSERT INTO TableName ( [Col1] )
                VALUES ( 'Col1StrVal' )

                "[Col1]" would have is_quoted=False and is also surrounded by (optional) brackets
                "Col1StrVal" would have is_quoted=True, since it is a string and needs quotes

        :param is_null_to_empty_string: bool = True
            If a null (or None) value or an empty string is passed in, this determines if it should be returned as
             either an empty string ('') or as a null (NULL)
        :return:
        """
        val = str(val)
        if not val or not val.strip():
            if is_null_to_empty_string:
                return '\'\''
            return 'NULL'

        val = re.sub("'", "''", val)

        # Don't need brackets for insert statements
        if is_quoted:
            return '\'' + val + '\''

        return str('[' + val + ']')

    @staticmethod
    def get_column_values(header, is_quoted: bool):
        import builtins as b

        if (type(header) == str and not header.strip()) or len(header) == 0:
            return

        if type(header) == b.str:
            header = (header,)

        sql = []
        try:
            for i, col in enumerate(header):
                if i == 0:
                    sql.append(' ' + str(SqlBuilder.__get_insert_value(col, is_quoted)))
                else:
                    sql.append(', ' + str(SqlBuilder.__get_insert_value(col, is_quoted)))
        # TODO: Can probably take out since encoding check is happening in createInsertStatement() \
        #          (may want to change and put in where a single val is escaped)
        except UnicodeDecodeError:
            return SqlBuilder.get_column_values(header, is_quoted)

        return ''.join(sql)

    # TODO: Get rid of appendGo - put this option in when building
    @staticmethod
    def create_insert_statement(tableName, database_name: str, colNames, insertVals, schema_name='', appendGo=True):
        # TODO: Surround datbase_name, schema_name, and tableName with brackets ([])
        # TODO: If schema isNullOrEmpty, don't surround with brackets
        tableName = tableName.strip()
        database_name = database_name.strip()
        schema_name = schema_name.strip()

        if schema_name:
            schema_name = '[{0}]'.format(schema_name)
        if database_name:
            database_name = '[{0}]'.format(database_name)
        if tableName:
            tableName = '[{0}]'.format(tableName)

        sql = str('INSERT INTO {0}.{1}.{2} (\n{3}\n)\nVALUES (\n{4}\n)\n') \
            .format(database_name,
                    schema_name,
                    tableName,
                    SqlBuilder.get_column_values(colNames, False),
                    SqlBuilder.get_column_values(insertVals, True)
                    )

        if appendGo:
            sql = '{0}GO'.format(sql)

        return sql


class DataImport(object):
    def __init__(self,
                 database_name: str,
                 schema_name: str,
                 table_name: str,
                 column_names: [],
                 rows: [],
                 options: str='SET NOCOUNT ON;\n',
                 is_row_contains_column_names: bool = False):
        self.database_name = database_name
        self.schema_name = schema_name
        self.table_name = table_name
        self.column_names = column_names
        self.options = options

        if is_row_contains_column_names:
            self.rows = rows[1:]
        else:
            self.rows = rows

    def generate_sql_options(self):
        return '\n{0}\n'.format(self.options)

    def generate_sql_create_table_stmt(self):
        """
        Description:
            Returns string with CREATE TABLE SQL statement
        :return: str
        """
        return SqlBuilder.create_table_from_header(table_name=self.table_name,
                                                   database_name=self.database_name,
                                                   header=self.column_names,
                                                   schema_name=self.schema_name)

    def generate_sql_insert_stmts_list(self):
        """
        Description:
            Returns list of SQL insert statements
        :return: []
        """
        stmts = []
        for r in self.rows:
            stmts.append('{0}{1}'.format(SqlBuilder.create_insert_statement(tableName=self.table_name,
                                                                            database_name=self.database_name,
                                                                            colNames=self.column_names,
                                                                            insertVals=r,
                                                                            schema_name=self.schema_name),
                                         '\n'))
        return stmts

    def generate_all_sql(self):
        """
        Description:
            Return string of CREATE TABLE and INSERT SQL statements
        :return:
        """
        sql = [self.generate_sql_options(), self.generate_sql_create_table_stmt()]
        for r in self.generate_sql_insert_stmts_list():
            sql.append(r)

        return '\n'.join(sql)

    def write_output_file(self, output_name: str = None, sql_cmd_script_name: str=None):
        if not output_name or not output_name.strip():
            output_name = '{0}.sql'.format(self.table_name)    

        with open(output_name, 'w') as f:
            f.write(self.generate_sql_options())
            f.write(self.generate_sql_create_table_stmt())
            for s in self.generate_sql_insert_stmts_list():
                try:
                    f.write(s)
                except Exception as e:
                    print('Failed to write line in file {0}:\n{1}\nException:\n{2}'.format(output_name, s, e))
        
        if sql_cmd_script_name:
            with open(sql_cmd_script_name, 'w') as f:
                f.write('USE master;\nGO\n')
                f.write('r: {0}'.format(os.path.abspath(output_name)))
        